<?php
/**
 * The main template file
 *
 *
 * */

get_header();
?>
	<section id="primary" class="standard-page side-page content-area">
		<main id="main" class="site-main">

<?PHP 
	//REGULAR Header with brown box
	get_template_part('components/heading-side');
?>
<div class="two-col">
    <div class="container">
        <div class="entry-content">
        <h2>
        Well this is uncomfortable, but the page you are looking for does not exist.  
        </h2>
        <?php
        the_content();
        ?>  
        </div>
        <?PHP 
            get_template_part('components/side-image');
        ?>
    </div>
</div>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php
get_footer();
