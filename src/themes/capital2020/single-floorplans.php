<?php
/**
 * The main template file
 *
 *
 * */

get_header();
?>
	<section id="primary" class="floorplan-page content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) {

			// Load posts loop.
			while ( have_posts() ) {
                the_post();

                ?>
<?PHP 
// GET ALL FP DATA
$fpdata = _cptheme_getFloorplanData($post);
$lsclass="portrait";
if($fpdata['landscape']){
	$lsclass="landscape";
}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header boxheader">
		<div class="container-wide">
			<div class="top-wrap">
			<?php
			the_title( '<h1 class="entry-title">', '</h1>' );
			?>
			<div class="room-info">
				<div class="box">
					<span class="beds"><?php 
					echo $fpdata['beds']; 
					if($fpdata['beds'] > 1){
						echo ' Bedrooms';
					}
					else{
						echo ' Bedroom';
					}
					
					?></span> - 
					<span class="baths"><?php 
					echo $fpdata['baths']; 
					if($fpdata['baths'] > 1){
						echo ' Bathrooms';
					}
					else{
						echo ' Bathroom';
					}
					?>
					</span>
				</div>
				
			</div>
			</div>
		</div>
		
	</header><!-- .entry-header -->
	<div class="floorplan-nav-container">
		<div class="container-wide fpn-container">
			<div class="backlink">
			<?PHP 
				//get fp landing page
				$fppage = get_field('suites_page','options');
				$fplink = (!empty($fppage)) ? get_the_permalink($fppage) : '';
				//get back button category
				
				$floorplan_backurl = $fplink.'#suites';
				$options = array(
					'attributes' => 'id="back-to-floorplans"'
				);
				echo _cptheme_buildBtn($floorplan_backurl,'Back to all', 'blueoutline');
			?>
			</div>
			
			<?PHP 
				get_template_part('components/floorplan-nav');
			?>
		</div>
		
	</div>
	<div class="entry-content">
		<div class="container-wide">
			<div class="fpcontainer <?PHP echo $lsclass; ?>">
				<?PHP 
					//GET floorplan display here				
					if(!empty($fpdata['img'])){	
				?>
				<div class="fpimage">
					<?PHP 
						echo _cptheme_buildImageStr($fpdata['img'],$fpdata['imgalt']);
					
					?>
					<div class="north">
					<?PHP 
					$northfile = $fpdata['north'];
					if(!empty($northfile)){

						echo _cptheme_buildImage($northfile);
					}
					?>

					</div>
				</div>
				<?PHP 
					} //end fp image
				?>
				<?php 
				if(!empty($fpdata['locationimg'])){
					?>
					<div class="floorplate">
					<?PHP echo _cptheme_buildImageStr($fpdata['locationimg'],$fpdata['locationalt']); ?>
					</div>
					<?php
				}
				?>
			</div>

		</div>
		<?php 
		$fpcontent = get_the_content();
			if(!empty($fpcontent)){

				?>
				<div class="details">
					<div class="container-wide">
						<div class="wrapper">
							<div class="scol-wrap">
								<div class="scol scol-4 title-col">
									Suite Amenities
								</div>
								<div class="scol scol-8">
									<div class="suitedetails">
									<?php echo apply_filters('the_content',$fpcontent); ?>
									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php

			}
		?>
		
		<?PHP 
			get_template_part('components/floorplan_slider');
		?>
		
		<div class="suite-footer">
			<div class="container-wide">
				<div class="wrapper">
				<div class="description">
					<div class="suitesummary">
					<?php
					echo $fpdata['title'];

					$separator = ', ';
					echo $separator ;

					$beds =  $fpdata['beds'];
					$baths = $fpdata['baths'];				
					$bedstring = ' Bedroom';
					if(intval($beds) > 1){
						$bedstring = ' Bedrooms';
					}

					echo $beds.$bedstring;
					echo $separator ;

					$bathstring = ' Bathroom';
					if(intval($baths)> 1){
						$bathstring = ' Bathrooms';
					}
					echo $baths.$bathstring;

					?>
					<span class="gold">
					<?PHP
					if(!empty($fpdata['typename'])){
						echo ' / ';
						echo $fpdata['typename'];
					}
					?>
					</span>
					
					</div>
					<?php get_template_part('components/sgnote'); ?>
				</div>
				<?PHP
				//get downlaod pdf
				$downloadlink = get_field('download');
				
				if(!empty($downloadlink)){
					$dlattr = array(
						'attributes' => 'id="download-'.$fpdata['slug'].'"',
					);
					echo '<div class="downloadlink">';
					echo _cptheme_buildbtn($downloadlink, 'Download Floorplan','red',$dlattr);
					echo '</div>';
				}
				?>
				</div>
				
			</div>
			

		</div>
		
	</div><!-- .entry-content -->
</article>
                <?php
			} //endwhile

		} else {

			// If no content, include the "No posts found" template.
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php _e( 'Page Not Found', '' ); ?></h1>
	</header><!-- .page-header -->
	<div class="page-content">
		<p><?php _e( 'This page is not found. Click on the main menu to find what pages we have!', '' ); ?></p>
	</div><!-- .page-content -->
</section><!-- .no-results -->

<?php

		} //end if
		?>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php
get_footer();
