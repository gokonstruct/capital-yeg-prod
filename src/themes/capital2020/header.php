<?php
/**
 * The header
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>

	<?PHP 
	if(function_exists('get_field')){
$devmode = get_field('devmode','options');
if(empty($devmode)){ //only show these if not in dev mode
?>
	<?php 
	//check google analytics
	$googletagid = get_field('google_analytics_id','options');
	if(!empty($googletagid)){
		?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','<?php echo $googletagid; ?>');
	
	</script>
	<!-- End Google Tag Manager -->
	<?php
	} //end google analytics check
	?>
	<?PHP 
} //end devmode check
} //check acf active
else{
	echo 'Missing Website Plugin';
}
?>
<!-- Global site tag (gtag.js) - Google Ads: 630956480 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-630956480"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-630956480'); </script>
</head>

<body <?php body_class(); ?>>
<?php 
//check google analytics
if(!empty($googletagid)  && empty($devmode)){
?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $googletagid; ?>"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php
	} //end google analytics check
?>
<?php wp_body_open(); ?>
<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content'); ?></a>

<header id="header" class="site-header">
<div class="head-wrapper">
	<div class="fluid-container">
		<div class="nav-container">
			
			<div class="logo-container" data-aos="fade-up">
				<a href="<?php echo get_site_url(); ?>" class="logo" id="logo-homepage">
				<!-- <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.svg" alt="<?php echo get_bloginfo('name'); ?>"> -->
				<div class="l1"><?PHP get_template_part('components/logo');?></div>
				<div class="stack"><?PHP get_template_part('components/logostack');?></div>
				</a>
			</div>
			<div class="right" data-aos="fade-down">
				<div class="desktop-container">
				<?php
					//show out nav
					wp_nav_menu( array(
						'theme_location'    => 'header-menu',
						'depth'             => 2,
						'container'         => 'div',
						//'container_class'   => 'collapse navbar-collapse',
						//'container_id'      => 'bs-example-navbar-collapse-1',
						'menu_class'        => 'nav navbar desktop-nav',
						'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
						'walker'            => new WP_Bootstrap_Navwalker(),
					) );
				?>
				</div>
				<ul class="hlist-style">
					<li><?PHP 
					$schattrs = array(
						'attributes'=> 'data-toggle="modal" data-target="#scheduletour" id="schedule-top"',
					);
					echo _cptheme_buildBtn('','Book a tour','',$schattrs);
					?></li>
					<li class="phone-btn desktop-lg"><?PHP 
						$phone = get_field('phone','options'); 
						if(!empty($phone)){
							$pnumber = preg_replace('~\D~', '', $phone);
							
							$phoneatts = array(
								'attributes'=> 'id="phone-top"',
							);
							echo _cptheme_buildBtn('tel:'.$pnumber,$phone,'redoutline',$phoneatts);
						}
					?></li>
				</ul>
				<div class="toggle-container">
				<div class="menutoggler">
						<div class="bar bar1"></div>
						<div class="bar bar2"></div>
						<div class="bar bar3"></div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="mobile-container">
			<div class="mobile-wrapper">

			<div class="top">

			
			<?php
				wp_nav_menu( array(
					'theme_location'    => 'header-menu',
					'depth'             => 2,
					'container'         => 'div',
					//'container_class'   => 'collapse navbar-collapse',
					//'container_id'      => 'bs-example-navbar-collapse-1',
					'menu_class'        => 'nav mobile-nav',
					'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
					'walker'            => new WP_Bootstrap_Navwalker(),
				) );
			?>
			<?PHP 
				$phone = get_field('phone','options'); 
				if(!empty($phone)){
					$pnumber = preg_replace('~\D~', '', $phone);
					
					$phoneatts = array(
						'attributes'=> 'id="phone-menu"',
					);
					?>
					<div class="phonewrap">
						<?PHP echo _cptheme_build_raw_link('tel:'.$pnumber,$phone,$phoneatts); ?>
					</div>
					<?php
					
				}
			?>
			</div>
			<div class="menu-footer">
				<div class="sg">
				Developed by Strategic Group.
				</div>
				<div class="pvt">
				<?php
                    $tcpage = get_field('tc_page','options');
                    if(!empty($tcpage)){
                        $tcurl = get_the_permalink($tcpage);
                        echo _cptheme_build_raw_link($tcurl,'Terms & Conditions');
                    }
                    if ( function_exists( 'the_privacy_policy_link' ) ) {
                        the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
                    }
                    ?>
				
				</div>
				
			</div>
			</div>
		</div>
	</div>
	

</div>
	
</header>
<?php get_template_part("components/sitemessage"); ?>
