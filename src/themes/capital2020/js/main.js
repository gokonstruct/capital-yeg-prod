//console.log("hello - loaded files");

jQuery(document).ready(function(){
    _cpt_init_smoothScrolls();
    _cpt_init_alerts();
    _cpj_init_menu();
    // AOS.init({
    //     duration: 1000,
    // }); //init animations
    
});

//setup sticky nav listener
jQuery(window).scroll(function(){
    var sticky = jQuery('.site-header'),
        scroll = jQuery(window).scrollTop();
  
    var navheight = sticky.outerHeight();
    //console.log(scroll);
    if (scroll >= navheight) sticky.addClass('sticky');
    else sticky.removeClass('sticky');
  });
  

  
function _cpj_init_menu(){
  var mymenu = '.site-header';
    if(jQuery(mymenu).length){
        //console.log("Load menu container files");
        jQuery(mymenu).on('click','.menutoggler', function(){
            jQuery(mymenu).toggleClass('active');
            jQuery(this).toggleClass('open');
        });


        //update spacing on header when not mobile?
        jQuery(window).on('resize', function() {
          //if (jQuery(window).width() > 600) {
            var menuheight = jQuery(mymenu).outerHeight();
            // console.log(menuheight);
            jQuery(document.body).css('padding-top',menuheight+'px');
          //}
        });
        
        
    }

}

  
//w3 smooth scroll
function _cpt_init_smoothScrolls(){
  jQuery("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    
    var href = this.attributes.href.value;
    var split = href.split('#');
    var root = split[0];
    var origin = window.location.origin + window.location.pathname;
    //console.log(root);
    //console.log(origin);
    if (this.hash !== "" && 
      root.trim() === origin.trim()  || root === ''
      ) {
      // Prevent default anchor click behavior

      
      event.preventDefault();
      // Store hash
      var hash = this.hash;
      jQuery('html, body').animate({
        scrollTop: jQuery(hash).offset().top
      }, 850, function(){
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  }

  function _cpt_init_alerts(){

    //gehck site messages
    if(jQuery('.site-message').length){
      var smref = '.site-message';
      jQuery(smref).alert();
      var poptrack = 'sitemessage';
      if(_upj_getCookie(poptrack)){
        //already seen // keep hidden
        jQuery(smref).hide();
        console.log('Message hidden');
      }
      else{
        setTimeout(
          function(){
            jQuery(smref).fadeIn();
            console.log('show alert');
          }, 750
        );
        
        jQuery(smref).on('closed.bs.alert', function () {
          // do something…
          _upj_setCookie(poptrack, 1, 0.5); 
          
          console.log('hide alert');
        })
         
        
      }

    }

    //set up promotion popup
    //do not show promotion on incentive page
    if (window.location.href.indexOf("incentives") > -1) {
      //contains incentive page
      //do not show 
      console.log('hide');
    }
    else if(jQuery('#promotionpop').length){
      var thepromo = '#promotionpop';
      //jQuery(thepromo).modal();

      console.log('activate promo');
      var promotracker = 'cpromo';
      if(_upj_getCookie(promotracker)){
      // if(false){
        //already seen // keep hidden
        console.log('promo hidden');
      }
      else{
        //show promo after 5 seconds
        setTimeout(
          function(){
            jQuery(thepromo).modal('show');
            console.log('show promo');
          }, 5000
        );
        
        jQuery(thepromo).on('hidden.bs.modal', function () {
          
          console.log('hide promo');
        });
        jQuery(thepromo).on('click','.close',function(){
          // do something…
          _upj_setCookie(promotracker, 1, 0.5); 
          console.log('close click');
        });
         
         
        
      }

    }
  }



  function _upj_setCookie(cname, cvalue, hours) {
    var d = new Date();
    d.setTime(d.getTime() + (hours*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
  function _upj_getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }