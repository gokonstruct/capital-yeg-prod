<?php
/**
 * The main template file
 *
 *
 * */

get_header();
?>
	<section id="primary" class="standard-page content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) {

			// Load posts loop.
			while ( have_posts() ) {
                the_post();

                ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?PHP 
	//REGULAR Header with brown box
	get_template_part('components/heading-simple');
?>

	<div class="entry-content">
		<div class="container-wide">
		<?php
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:' ),
				'after'  => '</div>',
			)
		);
		?>
		</div>
		
	</div><!-- .entry-content -->
</article>
                <?php
			} //endwhile

		} else {

			// If no content, include the "No posts found" template.
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php _e( 'Page Not Found', '' ); ?></h1>
	</header><!-- .page-header -->
	<div class="page-content">
		<p><?php _e( 'This page is not found. Click on the main menu to find what pages we have!', '' ); ?></p>
	</div><!-- .page-content -->
</section><!-- .no-results -->

<?php

		} //end if
		?>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php
get_footer();
