<?php

//set up custom posts
require_once dirname( __FILE__ ) . '/inc/custom-content/post-types.php';
//set up custom fields specific to theme
require_once dirname( __FILE__ ) . '/inc/custom-content/custom-fields.php';

// Register Custom Navigation Walker
if ( ! file_exists( get_template_directory().'/inc/class-navwalker.php' ) ) {
    // file does not exist... return an error.
    return new WP_Error( 'class-wp-bootstrap-navwalker-missing', __( 'It appears the class-navwalker.php file may be missing.', 'wp-bootstrap-navwalker' ) );

} else {
    // file exists... require it.
	require_once get_template_directory().'/inc/class-navwalker.php';
}

//enqueue styles / scripts
function _cptheme_enqueue_scripts() {

    //defaults and required assets first
    //icons don't exist here...
    //wp_enqueue_style( 'up-icons', get_stylesheet_directory_uri() . '/css/styles.css', array(), date('H:i:sa') );

    //icons - can check if using others...
    //wp_enqueue_style('fa-fonts','https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style( 'fa-fonts', get_stylesheet_directory_uri() . '/css/fa.min.css', array(), '5.11' );

    
    //slider
    wp_enqueue_style( 'swiper-style', get_stylesheet_directory_uri() . '/css/swiper.min.css', array() );

    //aos
    // wp_enqueue_style( 'aos-style', get_stylesheet_directory_uri() . '/css/aos.css', array() );
    // wp_enqueue_script( 'aos-scripts', get_stylesheet_directory_uri() . '/js/aos.js', array('jquery') );

    
    wp_enqueue_style('google-fonts','https://fonts.googleapis.com/css2?family=Cabin:wght@400;700&family=Lora:ital,wght@0,400;0,500;1,500&display=swap', array(),null);

    //theme specific
    //wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'cp-style', get_stylesheet_directory_uri() . '/css/main.css', array(), date('H:i') );

    //load js files here
    //popperjs
    wp_enqueue_script( 'popper-scripts', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array('jquery') );
    //bootstrap?
    wp_enqueue_script( 'bs-scripts', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery') );
    
    
    wp_enqueue_script( 'swiper-scripts', get_stylesheet_directory_uri() . '/js/swiper.min.js', array('jquery') );
    
    //lightbox script
    wp_enqueue_style( 'ekl-style', get_stylesheet_directory_uri() . '/css/ekko-lightbox.css', array() );
    wp_enqueue_script( 'ekl-scripts', get_stylesheet_directory_uri() . '/js/ekko-lightbox.min.js', array('jquery') );
    


    $jsvariables = array(
      'siteurl' => get_option('siteurl'),
      'bpage' => get_query_var('paged'),
      'nnce' => wp_create_nonce('bldnnce'),
      'ajax_url' => admin_url( 'admin-ajax.php' ),
    );
    wp_enqueue_script( 'cp-script', get_stylesheet_directory_uri() . '/js/main.js', array('jquery'), date('H:i:sa') );
    wp_localize_script('cp-script', 'WNURLS', $jsvariables);

}
add_action( 'wp_enqueue_scripts', '_cptheme_enqueue_scripts' );


function _cptheme_init_stuff(){
  add_theme_support( 'title-tag' );  //allow default titles
  // This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'header-menu' => __( 'Header Menu', 'cp' ),
				//'footer' => __( 'Footer Menu', 'cp' ),
			)
        );
        add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
        //add default image sizes
        //add_image_size( string $name, int $width, int $height, bool|array $crop = false );
        add_image_size( "CP Feature Image", 2000, 1000, false );

}
add_action( 'after_setup_theme', '_cptheme_init_stuff' );


add_filter( 'excerpt_length', function($length) {
  return 20;
} );


//THEME HTML Functions
function _cptheme_build_raw_link($link = '', $title = '', $options = array()){
  $href = strval($link);
  if(empty($title)){
    $title = 'Learn More';
  }

  $attributes = '';
  if(!empty($options)){
    if(isset($options['attributes'])){
      $attributes = $options['attributes'];
    }
  }

  //create output;
  $output = <<<SET
<a href="$href" $attributes ><span>$title</span></a>
SET;

    return $output;
}
function _cptheme_buildbtn($link = "", $title = "Learn More", $color = '', $options = array()){

  $href = strval($link);
  if(empty($title)){
    $title = 'Learn More';
  }
  //setup options
  $attributes = '';
  if(!empty($options)){

    
    if(isset($options['attributes'])){
      $attributes = $options['attributes'];
    }
    
    if(isset($options['type'])){
      if($options['type'] == 'div'){
        return $output = <<<SET
<div class="btn $color" $attributes><span>$title</span></div>
SET;
      }
      elseif($options['type'] == 'arrow'){
        $imgpath = get_stylesheet_directory_uri().'/images/';
        $iconstyle = 'triangle.svg';
        $iconpath = $imgpath.$iconstyle;

        $imghtml = '<img src="'.$iconpath.'" />';
        return $output = <<<ARW
<a href="$href" class="btn $color" $attributes ><span class="llabel">$title</span><span class="icon">$imghtml</span></a>
ARW;
      }  
    }

    
  }

  //create output;
  $output = <<<SET
<a href="$href" class="btn $color" $attributes ><span>$title</span></a>
SET;

    return $output;
}

function _cptheme_buildbtnCross($href = "#", $color = ''){
    $output = <<<SET
<a href="$href" class="btn plus $color"><i class="fa fa-plus"></i></a>
SET;

    return $output;
}

//create image link
function _cptheme_genImage($url,$title, $iclass = ''){
  $output = '';
  if(empty($url)){
    return 'no img';
  }
  $output = '<img src="'.$url.'" alt="'.$title.'" class="'.$iclass.'">';

  return $output;
}



//AJAX

//breadcrumb code?

/**
 * Generate breadcrumbs
 * @author CodexWorld
 * @authorURL www.codexworld.com
 */
function _cptheme_get_breadcrumb() {
  global $post;
  $imgpath = get_stylesheet_directory_uri().'/images/';
  $iconstyle = 'triangle.svg';
  $iconpath = $imgpath.$iconstyle;
  $nextarrow = "<span class=\"sep\"> | </span>";
  
    echo '<a href="'.home_url().'" rel="nofollow"><span class="icon"><img src="'.$iconpath.'"></span><span>Home</span></a>';
    if (is_category() || is_single()) {
        // if(strcmp($post->post_type,'courses') == 0){
        //   echo $nextarrow;
        //   echo '<a href="'.get_site_url().'/courses">'.'Learning Communities'.'</a>';
        // }
        if (is_single()) {
          
          
           
        }
    } elseif (is_page()) {
      
      
        
        echo $nextarrow;
        echo the_title();
      
        
    } elseif (is_search()) {
        echo "$nextarrow Search Results for... ";
        echo '"<em>';
        echo the_search_query();
        echo '</em>"';
    }

    
}

//build image tag with given img array
//array( url => '', alt='', title='')
function _cptheme_buildImage($data = array()){

  if(empty($data)){
    return '';
  }
  $url = $data['url'];
  $alt = isset($data['alt']) ? $data['alt'] : $data['title'] ;
  $imgstr = '<img src="'.$url.'" alt="'.$alt.'">';
  return $imgstr;
}

//build image with strings arguments

function _cptheme_buildImageStr($imgurl, $imgalt){
  $imgstr = '<img src="'.$imgurl.'" alt="'.$imgalt.'">';
  return $imgstr;
}

//output formatted wysiwyg text from ACF
function _cptheme_getParagraph($data){
  if(!empty($data)){
    $formatted = apply_filters('the_content',$data);
    return $formatted;
  }

  return '';
}


//build left column links
function _cptheme_build_link($key = '', $data, $targetprefix = 'item'){

  if(empty($data)){
    return '';
  }
  $title = $data['title'];


  $cclass = ($key == 0) ? '': 'collapsed'; 

  $output = <<<ITE
<div class="acitem $cclass" 
  data-key="item-$key" 
  data-toggle="collapse" 
  data-target="#$targetprefix-$key" 
  aria-expanded="false"
  aria-controls="item-$key"
  >
  <div class="text">$title</div><div class="icon"><div class="o">+</div><div class="c">-</div></div>
</div>
ITE;

  return $output;
}
//build right column accordion content
function _cptheme_build_acright($key,$data, $targetprefix = 'item'){
  
  if(empty($data)){
    return '';
  }

  $cclass = ($key == 0)? 'collapse show': 'collapse';
  $heading = $data['heading'];
  $content = $data['text'];
  $parentid = "parentacc";
  if(!empty($targetprefix)){
    $parentid = $targetprefix.'-parentacc';
  }

  $link = $data['link'];
  $lout = '';
  if(!empty($link)){
    $lout = _cptheme_buildbtn($link,'','orange');
  }


  $output = <<<RAI
  <div class="accontent $targetprefix-$key $cclass" id="$targetprefix-$key" 
    data-parent="#$parentid"
  >
    <h2>$heading</h2>
    <div class="text">
      $content
    </div>
    <div class="link-container">
        $lout
    </div>
  </div>
RAI;

  return $output;
}



//AJAX
//get paged data of courses specific to theme
function _cptheme_ajax_get_posts(){

  $nonce = $_POST['bld_ncn'];
  if ( ! wp_verify_nonce( $nonce, 'bldnnce' ) ){
    die ( 'Busted!');
  }
  //check filters and nonce
  $base = $_POST['base'];
  $postfilters = $_POST['filters'];
  $topic = '';
  if(isset($postfilters['category'])){
    $topic = $postfilters['category'];
  }
  $page = $postfilters['page'] ? $postfilters['page'] : 1;

  $filters = array();
  if(!empty($topic)){
    $filters['category'] = sanitize_text_field($topic);
  }
  if(!empty($page)){
    $filters['page'] = sanitize_text_field($page);
  }
  $data = _cptheme_getBlogPage($filters, $base);
  $output = $data['output'];
  $pager =  $data['pager'];

  $result = true;
  if(empty($output)){
    $result = false;
  }
  $outputarray = array(
    'error' => '',
    'result' => $result,
    'pager' => $pager,
    'output' => $output,
  );
    echo json_encode($outputarray);
		//echo $outputarray;
    wp_die();

}
add_action('wp_ajax_themeGetPosts','_cptheme_ajax_get_posts');
add_action('wp_ajax_nopriv_themeGetPosts', '_cptheme_ajax_get_posts');


//Query all posts given specific Filters
function _cptheme_get_posts($filters = array(), $page = 1){

  $taxquery = array();
  $filterarrays = array();
  if(_cptheme_checkexist($filters,'category') && $filters['category'] !== '-1'){
    $terms = (!is_array($filters['category'])) ? array($filters['category']) : $filters['category'];
        
    $typefilter = array(
      'taxonomy'	 	=> 'category',
      'field' => 'slug',
      'terms'	  	=> $terms,
      'compare' 	=> 'IN',
    );
    $filterarrays[] = $typefilter;

    //run through list of filters until the end

    $taxquery = array($filterarrays);
  }

  $page = (isset($page)) ? $page : get_query_var( 'paged' );
  $args = array (
      'post_type' => 'post',
      'post_status' => 'publish',
      'posts_per_page' => 6,
      'paged'          => $page,
      //default post sort?
      'tax_query'	=> $taxquery,

  );
  $query = new WP_Query( $args );

  return $query;

}

//get full blog list with pager
function _cptheme_getBlogPage($filters, $base = ''){
  $cpage = $filters['page'] ? $filters['page'] : 1;
  unset($filters['page']);
  $bquery = _cptheme_get_posts($filters, $cpage);
  
  
  $output = '';
  $pager = '';
  if($bquery->have_posts()){
      $theposts = $bquery->posts;
      $output = _cptheme_buildBlogList($theposts);

      $prevp = $cpage - 1;
      $nextp = $cpage + 1;
      $fallback_base = str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) );
      $thebase = !empty($base) ? $base: $fallback_base;
      $pager = paginate_links( array(
        'base'         => $thebase,
        'total'        => $bquery->max_num_pages,
        'current'      => max( 1, $cpage),
        'format'       => '?paged=%#%',
        'show_all'     => false,
        'type'         => 'list',
        'end_size'     => 2,
        'mid_size'     => 1,
        'prev_next'    => true,
        'prev_text'    => sprintf( '<i class="bicon prev" data-p="%2$d">%1$s</i>', __( 'Previous', 'text-domain' ),$prevp ),
        'next_text'    => sprintf( '<i class="bicon next" data-p="%2$d">%1$s</i>', __( 'Next', 'text-domain' ),$nextp ),
        'add_args'     => false,
        'add_fragment' => '',
      ) );
  }

  $data = array(
    'output' => $output,
    'pager' => $pager,
  );
  /* Restore original Post Data */
  wp_reset_postdata();

  return $data;

}

//build the output of all the post squares
function _cptheme_buildBlogList($postlist){

  $output = array();
  if(!empty($postlist)){
    foreach($postlist as $apost){
      $pdata = _cptheme_buildPostData($apost);
      $output[] = _cptheme_buildBlogSquare($pdata);
    }
  }
  return $output; 
}



//check empty / isset
function _cptheme_checkexist($data, $ref){
  if(isset($data[$ref]) && !empty($data)){
    return true;
  }
  return false;
}

//set up post with all data formatted in simple array.
function _cptheme_buildPostData($bpost){
  $output = array();
  
  if(!empty($bpost)){
    $postid = $bpost->ID;
    $title = $bpost->post_title;
    $content = $bpost->post_content;
    $excerpt = get_the_excerpt($bpost);
    $categories = get_the_category($bpost);
    $link = get_the_permalink($bpost);
    $imageurl = get_the_post_thumbnail_url( $bpost, 'UP Feature Image' );

    
    $output = array(
      'title' => $title,
      'id' => $postid,
      'content' => $content,
      'excerpt' => $excerpt,
      'categories' => $categories,
      'image' => $imageurl,
      'link' => $link,
    );

  }

  return $output;
}

//set up all blog data into html
function _cptheme_buildBlogSquare($postdata){


  //setup data
  $output = '';
  if(!empty($postdata)){

  
  extract($postdata);

  $imgstr = '';
  if(isset($image) && !empty($image)){
    $imgstr = "<img src=\"$image\" alt=\"$title\">";
  }
  $cstring = '';
  $cnamearray = array();
  if(is_array($categories)){
    foreach( $categories as $ac){
      $tname = $ac->name;
      $tid = $ac->term_id;
      $slug = $ac->slug;
      
      $cnamearray[] = $tname;
    }
    $cstring = implode(', ',$cnamearray);
  }

  $exstring = strip_tags($excerpt);
  $linkhtml = _cptheme_buildbtn($link,'Read More','black');

  $output = <<<BDAT
<article class="ablog" data-aos="fade-up" data-aos-duragion="800">
  <div class="blog-wrap">
  <div class="top">
      <div class="imgwrap">
          $imgstr
      </div>
  </div>
  <div class="bottom">
      <div class="text-wrap">
          <div class="cat">$cstring</div>
          <h4 class="btitle"><a href="$link">
          $title
          </a></h4>
          <div class="text">
          $exstring
          </div>
      </div>
      <div class="link-container">
        $linkhtml
      </div>
  </div>
  </div>
</article>
BDAT;

  }

  return $output;

}


//get floorplans

//Query all posts given specific Filters
function _cptheme_getFloorplans($filters = array()){

  $taxquery = array();
  $filterarrays = array();
  if(_cptheme_checkexist($filters,'category') && $filters['category'] !== '-1'){
    $terms = (!is_array($filters['category'])) ? array($filters['category']) : $filters['category'];
        
    $typefilter = array(
      'taxonomy'	 	=> 'building',
      'field' => 'slug',
      'terms'	  	=> $terms,
      'compare' 	=> 'IN',
    );
    $filterarrays[] = $typefilter;
    //run through list of filters until the end
    $taxquery = array($filterarrays);
  }

  $args = array (
      'post_type' => 'floorplans',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'orderby' => 'title',
      'order'   => 'ASC',
      //default post sort?
      'tax_query'	=> $taxquery,

  );
  $query = new WP_Query( $args );
  if($query->have_posts()){
    return $query->posts;
  }
  
  return array();

}

//get floorplan data


//Set up partner data
function _cptheme_getFloorplanData($afloorplan){
  //Set up partner data
  // print_R($afloorplan);
   $fid = $afloorplan->ID;
   $slug = $afloorplan->post_name;
   $title = $afloorplan->post_title; 
  $link = get_the_permalink($fid);

  $name = $title; //get_field();

  $bed = get_field('beds',$fid);
  $bath = get_field('bath',$fid);
  
  $location = get_field('fploc',$fid);
  $locimg = '';
  $localt = '';
  if(!empty($location)){
    $locimg= $location['url'];
    $localt = $location['alt'] ? $location['alt'] : $location['title'];
  }

  $gallery = get_field('gallery', $fploc);

  $imgobj = get_field('fpimg', $fid);
   $imgurl = '';
   $imgalt = '';

   $islandscape = true; 
   if(!empty($imgobj)){
     $imgurl = $imgobj['url'];
     $imgalt = $imgobj['alt'] ? $imgobj['alt'] : $imgobj['title'];
    
     $islandscape = ($imgobj['width'] > $imgobj['height']) ? true : false;
   }

   //get building category?
    $fp_type = get_the_terms( $afloorplan, 'building');
    $typeslug = '';
    $typename = '';
    $firstterm = null;
    if(!empty($fp_type)){
      $firstterm = reset($fp_type);
      $typeslug = $firstterm->slug;
      $typename = $firstterm->name;
    }


    //north image
    $north = get_field('north',$fid);
    

   $pdata = array(
     'title' => $title,
     'name' => $name,
     'slug' => $slug,
     'link' => $link,
     'img' => $imgurl,
     'imgalt' => $imgalt,
     'landscape' => $islandscape,
     'beds' => $bed,
     'baths' => $bath,
     'locationimg' => $locimg,
     'locationalt' => $localt,
     'gallery'=>$gallery,
     'typename' => $typename,
     'typeslug' =>$typeslug,
     'north' => $north,

   );
   return $pdata;
 
 }

 function _cptheme_generateFloorplanSquare($fdata){

    $ftitle = $fdata['title'];
    $fslug = $fdata['slug'];
    $flink = $fdata['link'];
    $fimg = $fdata['img'];
    $beds = $fdata['beds'];
    $baths = $fdata['baths'];
    $sqft = $fdata['squarefeet'];
    $type = $fdata['typename'];
    $imgstring ='';

    if(!empty($flink)){
        $ftitle = '<a href="'.$flink.'" id="'.$fslug.'-link">'.$ftitle.'</a>';
    }

    if(!empty($fimg)){
      $imgtag = _cptheme_buildImageStr($fimg,$fdata['imgalt']);
      $imgstring = '<a href="'.$flink.'" id="'.$fslug.'-image-link">'.$imgtag.'</a>';
    }

    $bedstring = 'Bedroom';
    if(intval($beds) > 1){
        $bedstring = 'Bedrooms';
    }
    $bathstring = 'Bathroom';
    if(intval($baths)> 1){
        $bathstring = 'Bathrooms';
    }

    $flink = $fdata['link'];

    $buttonstring = _cptheme_buildbtn($flink,'View Details','blueoutline');

  $output = <<<FPS
<article class="afp" >
  <div class="fp-wrap">
      <div class="img-container">
          $imgstring
      </div>
      <div class="text-container">
          <div class="text">
              <h4 class="name">$ftitle</h4>
              <div class="bb">
                  <div class="tower">$type</div>
                  <div class="beds">$beds $bedstring</div>
                  <div class="baths">$baths $bathstring</div>
              </div>
          </div>
          <div class="link-container">
            $buttonstring
          </div>
      </div>
  </div>
</article>
FPS;

  return $output; 
 }