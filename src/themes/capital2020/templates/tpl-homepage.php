<?php
/*
    Template Name: Homepage
*/
?>
<?php get_header(); ?>
<?php
if ( have_posts() ) {

    // Load posts loop.
    while ( have_posts() ) {
        the_post();

        ?>
<div class="home-wrap full-home">
    <?PHP 
    get_template_part('components/home-heading');
    get_template_part('components/home-getanywhere');

    //suites version with text and slider
    get_template_part('components/home-suites');
    //image slider

    //Building amenity description
    //and slider
    get_template_part('components/home-amenities');


    ?>
    
</div>
<?php
    } //endwhile

} 
?>
<?php get_footer(); ?>