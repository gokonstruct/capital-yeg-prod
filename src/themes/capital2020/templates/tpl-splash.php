<?php
/*
    Template Name: Splash
*/
?>
<?php get_header(); ?>
<div class="home-wrap splash-page">
    <?PHP 
    get_template_part('components/home-heading');
    get_template_part('components/getanywhere');
    get_template_part('components/suityou');
    get_template_part('components/homeimg');
    ?>
    
</div>
<?php get_footer(); ?>