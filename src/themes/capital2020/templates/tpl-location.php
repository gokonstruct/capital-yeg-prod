<?php
/*
    Template Name: Location
*/
?>
<?PHP
get_header();
?>
	<section id="primary" class="location-page">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) {

			// Load posts loop.
			while ( have_posts() ) {
                the_post();

                ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?PHP 
	//REGULAR Header with brown box
	get_template_part('components/heading-box');
?>
	
	<div class="entry-content">
        <section class="score-section" id="score">
        <div class="container">
            <div class="score-wrapper">
                <div class="scol-wrap sc-middle">
                <div class="scol scol-3 link-col">
                    <div class="link-container">
                    <?php
                        //get amenities links
                        $amid = get_field('ammenities_page','options');
                        if(!empty($amid)){
                            $amenitiesl = get_the_permalink($amid);
                            $amoptions = array(
                                'attributes' =>'id="loc-amenities-page"',
                            );
                            echo _cptheme_buildbtn($amenitiesl,'Building Amenities','blueoutline',$amoptions);
                        }
                    ?>
                    </div>
                </div>
                <div class="scol scol-9 score-col">
                <?PHP 
            $scores = get_field('scores');
            if(!empty($scores)){
                
            ?>
            <div class="walkscore">
                <?PHP 
                foreach($scores as $ascore){
                    $thescore = $ascore['score'];
                    $slabel = $ascore['type'];
                    $sdesc = $ascore['desc'];
                    ?>
                    <div class="as">
                    <div class="score"><?PHP echo $thescore; ?></div>
                    <div class="text-container">
                        <div class="slabel"><?PHP echo $slabel; ?>
                        </div>
                        <div class="stype"><?PHP echo $sdesc; ?>
                        </div>
                    </div>
                </div>
                    <?PHP
                }
                ?>
                
            </div>

            <?PHP
            }   //end scores
            ?>
                </div>
            
            
            </div>
            
            </div>
        </div>
        </section>
        <section class="map-section" id="map">
            <div class="container">
                <div class="mapfile">
                    <?PHP 
                    $mapimage = get_field('mapimage');
                    if(!empty($mapimage)){

                        $mapstring = _cptheme_buildImage($mapimage);
                        echo $mapstring;
                    }
                    ?>
                </div>
                <div class="download-section">
                    <div class="scol-wrap sc-middle">
                        <?php 
                        $locationaddress = get_field('locaddress');
                        $locationregion = get_field('locregion');

                        if(empty($locationaddress)){
                            $locationaddress = get_field('address','options'); 
                        }

                        ?>
                        <div class="scol scol-8 address-col">
                        <?php echo $locationaddress; ?> <span class="gold">/ <?php echo $locationregion; ?></span>
                        </div>
                        <div class="scol scol-4"> 
                            <div class="link-container">
                                <?php 
                                
                                $amenitiesl = get_field('amenities_file','options');
                                if(!empty($amenitiesl)){
                                    $aattrs = array(
                                        'attributes'=> 'target="_blank" id="map-download"',
                                    );
                                    echo _cptheme_buildBtn($amenitiesl,'Download Map','red',$aattrs);
                                } 
                                ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <section class="break-section">
            <div class="container">
                <div class="heading">
                <?php 
                    $middletext = get_field('middleheading');
                    echo $middletext;
                ?>
                
                </div>
            </div>
        </section>
        <?PHP          
        //Text title with story gallery
        get_template_part('components/style_gallery');

        ?>

	</div>
	
</article>
                <?php
			} //endwhile

		} else {

			// If no content, include the "No posts found" template.
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php _e( 'Page Not Found', '' ); ?></h1>
	</header><!-- .page-header -->
	<div class="page-content">
		<p><?php _e( 'This page is not found. Click on the main menu to find what pages we have!', '' ); ?></p>
	</div><!-- .page-content -->
</section><!-- .no-results -->

<?php

		} //end if
		?>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php
get_footer();
