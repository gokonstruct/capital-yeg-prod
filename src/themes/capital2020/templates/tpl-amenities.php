<?php
/*
    Template Name: Amenities
*/
?>
<?PHP
get_header();
?>
	<section id="primary" class="amenities-page">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) {

			// Load posts loop.
			while ( have_posts() ) {
                the_post();

                ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?PHP 
	//Image based header
	get_template_part('components/heading-image');
?>
	

	<div class="entry-content">
		  <section class="leadbox">
            <div class="container">
                <div class="scol-wrap">
                
                    <div class="scol scol-6">
                    
                        <div class="textdescription">
                        <h2>Take your life <br>
                        where it leads you.</h2>
                            <div class="description ind">
                                <?php 
                                    $description = get_field('leadtext');
                                    if(!empty($description)){
                                        echo apply_filters('the_content', $description);
                                    }
                                    ?>
                            </div>
                        </div>
                    </div>
                    <div class="scol scol-6">
                        
                        <?php 
                        //get building features
                        $bfeatures = get_field('buildingfeatures');
                        if(!empty($bfeatures)){
                            
                            ?>
                        <div class="buildingfeatures" id="features">
                            <div class="wrap">
                                <h4>Building Features</h4>
                                <ul>
                                <?PHP 
                                foreach($bfeatures as $af){
                                    echo '<li>'.$af['label'].'</li>';
                                }
                                ?>
                                </ul>
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>


            </div>
          </section>

          <?PHP 
          get_template_part('components/building-gallery');
          ?>
        <section class="viewlocation">
            <div class="container">
                <div class="linewrap">
                    
                    <div class="line">
                        Located On Beautiful Capital Boulevard <span class="gold"> / Downtown Edmonton</span>
                    </div>
                    <?php 
                    $locid = get_field('location_page','options');
                    if(!empty($locid)){
                        $locationsl = get_the_permalink($locid);
                        $lattrs = array(
                            'attributes'=> 'id="amenites-location-page"',
                        );
                        echo _cptheme_buildBtn($locationsl,"View Location",'',$lattrs);
                    } 
                    ?>
                </div>
            </div>
        </section>

        <section class="break-section">
            <div class="container">
                <div class="heading">
                <?php 
                    $middletext = get_field('middleheading');
                    echo $middletext;
                ?>
                </div>
            </div>
        </section>
        <?PHP          
        //Text title with story gallery
        get_template_part('components/style_gallery');
        ?>

	</div>
	
</article>
                <?php
			} //endwhile

		} else {

			// If no content, include the "No posts found" template.
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php _e( 'Page Not Found', '' ); ?></h1>
	</header><!-- .page-header -->
	<div class="page-content">
		<p><?php _e( 'This page is not found. Click on the main menu to find what pages we have!', '' ); ?></p>
	</div><!-- .page-content -->
</section><!-- .no-results -->

<?php

		} //end if
		?>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php
get_footer();
