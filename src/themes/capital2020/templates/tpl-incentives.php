<?php
/*
    Template Name: Incentives
*/
?>
<?PHP
get_header();
?>
	<section id="primary" class="side-page incentives-page">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) {

			// Load posts loop.
			while ( have_posts() ) {
                the_post();

                ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?PHP 
	//REGULAR Header with brown box
	get_template_part('components/heading-side');
?>
    
	<div class="two-col">
        <div class="container-wide">
            <div class="entry-content">
                <div class="maincontent">
                    <?php
                    the_content();
                    ?>

                    <?php
                    // book a tour link here
                    ?>
                    <div class="bat-link">
                        <?php 
                        $bookattrs = array(
                            'attributes'=> 'data-toggle="modal" data-target="#scheduletour" id="schedule-incentive"',
                        );
                        echo _cptheme_buildBtn('#','Book a Tour','',$bookattrs);
                        ?>
                        <div class="capital-text">
                            Experience Comfortable Living
                        </div>
                    </div>
                </div>
                <?php
                    get_template_part('components/link-footer');
                ?>
                <?php 
                //address and sg note

                ?>
                <div class="incfooter">
                    <div class="description">
                        <div class="addresssummary">
                            10043 108 Street North West
                            <span class="gold">/ Downtown Edmonton</span>
                        </div>
                        
    
                    </div>
                    <?php get_template_part('components/sgnote'); ?>

                </div>
            </div>
            <?PHP 
                get_template_part('components/side-image');
            ?>
        </div>
    </div>
</article>
                <?php
			} //endwhile

        } 
        
        
?>
		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php
get_footer();
