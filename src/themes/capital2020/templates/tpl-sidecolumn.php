<?php
/*
    Template Name: Side Template
*/
?>
<?PHP
get_header();
?>
	<section id="primary" class="side-page">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) {

			// Load posts loop.
			while ( have_posts() ) {
                the_post();

                ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?PHP 
	//REGULAR Header with brown box
	get_template_part('components/heading-side');
?>
    
	<div class="two-col">
        <div class="container-wide">
            <div class="entry-content">
                <div class="maincontent">
                    <?php
                    the_content();
                    ?>
                </div>
                <?php
                    get_template_part('components/link-footer');
                ?>

            </div>
            <?PHP 
                get_template_part('components/side-image');
            ?>
        </div>
    </div>
</article>
                <?php
			} //endwhile

        } 
        
        
?>
		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php
get_footer();
