<?php
/*
    Template Name: Contact
*/
?>
<?php
/*
    Template Name: Location
*/
?>
<?php
/*
    Template Name: Amenities
*/
?>
<?PHP
get_header();
?>
	<section id="primary" class="contact-page">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) {

			// Load posts loop.
			while ( have_posts() ) {
                the_post();

                ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?PHP 
	//REGULAR Header with brown box
	get_template_part('components/heading-box');
?>
	
	<div class="entry-content">
		  
        <div class="container">
            <div class="scol-wrap">
                <div class="scol scol-8 form-col">
                    <div class="form-container">
                
                        <?PHP 
                        echo do_shortcode('[gravityform id="3" title="false" /]');
                        ?>
                    </div>
                </div>
                <div class="scol scol-4 info-col">

                <div class="contact-info">
                <div class="abox">
                    <h5>Contact Us</h5>
                    <div class="address">
                    <?php echo get_field('address','options'); ?>
                    </div>
                    <?PHP 
                    if(!empty($phone)){
                        $pnumber = preg_replace('~\D~', '', $phone);
                        ?>
                        <div class="phonenumber">
                            <a href="tel:<?PHP echo $pnumber; ?>" class="phone" id="phone-contact"><?PHP echo $phone; ?></a>
                        </div>
                        
                        <?PHP
                    }
                    ?>
                    
                </div>
                <div class="abox">
                        <?PHP 
                            $checkhours = get_field('showoffice','options');
                            $hours = get_field('officehours','options');                    
                            //option to hide temporarily
                            if(!empty($checkhours) && !empty($hours)){
                                ?>
                                <h5>Leasing centre information</h5>
                                <h3>Hours of Availability</h3>
                                <div class="hourlist">
                                <?php 
                                foreach($hours as $hr){
                                    $d = $hr['day'];
                                    $t = $hr['time'];
                                    ?>
                                    <div class="timeset">
                                        <div class="day"><?PHP echo $d; ?></div>
                                        <div class="time"><?PHP echo $t; ?></div>
                                    </div>
                                    <?php 
                                } //end list

                                ?>
                                </div>
                                <?PHP 
                            }
                        ?>
                        
                </div>
            </div>
                </div>
            </div>
            
            
        </div>
	</div>
	
</article>
                <?php
			} //endwhile

		} else {

			// If no content, include the "No posts found" template.
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php _e( 'Page Not Found', '' ); ?></h1>
	</header><!-- .page-header -->
	<div class="page-content">
		<p><?php _e( 'This page is not found. Click on the main menu to find what pages we have!', '' ); ?></p>
	</div><!-- .page-content -->
</section><!-- .no-results -->

<?php

		} //end if
		?>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php
get_footer();
