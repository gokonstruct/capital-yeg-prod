<div class="fp-navigator">
	
	<div class="ct prevlink">
		<?PHP 
			$linktext = '<span class="arrow left"></span> <span class="text">Previous</span>';
			if( $ppost = get_adjacent_post(false, '', true) ) {
				echo previous_post_link( '%link', $linktext );
			} else {
				//loop to last post
				$first = new WP_Query("post_type=floorplans&posts_per_page=1&order=DESC&orderby='title'");
				$first->the_post();
				echo '<a href="' . get_permalink() . '" class="arrlink" id="previous-floorplan" >'.$linktext.'</a>';
				wp_reset_query();
			};
		?>
	</div>
	<div class="ct nextlink">
		<?PHP 
		$linktext = '<span class="text">Next</span><span class="arrow right"></span>';
		if($npost = get_adjacent_post(false,'',false)){
			echo next_post_link( '%link', $linktext);
		}
		else {
			//loop to first post
			$last = new WP_Query("post_type=floorplans&posts_per_page=1&order=ASC&orderby='title'");
			$last->the_post();
			echo '<a href="' . get_permalink() . '" class="arrlink" id="next-floorplan">'.$linktext.'</a>';
			wp_reset_query();
		};
		?>
	</div>
</div>