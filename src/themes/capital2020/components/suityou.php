<section class="suites-section" id="suites">
    <div class="container">
        <div class="scol-wrap">
            <div class="scol scol-4 heading-col">
                <div class="text">
                    <h2>Suites that <br>suit you.</h2>
                    <div class="link-container">
                        <?PHP 
                        //generate button to item or page based on where it's included
                        $floorplansl = get_field('floorplans_file','options');
                        if(!empty($floorplansl)){
                            $fpoptions = array(
                                'attributes' =>'target="_blank" id="floorplans-download"',
                            );
                            echo _cptheme_buildbtn($floorplansl,'View Floor Plans','red',$fpoptions);
                        }
                        ?>  
                    </div>

                </div>
            </div>
            <div class="scol scol-8 desc-col">
                <div class="text-container description sp">   
                    <?PHP 
                    $suitesummary_raw = get_field('suitetext');
                    $suitesummary = apply_filters('the_content',$suitesummary_raw);
                    echo $suitesummary;
                    ?>
                    <!-- <p>Each morning, stretch out in your king size bed and enjoy the contemporary  features at Capital. Each suite is pet friendly and includes walk-in closets, in suite laundry and spacious layouts so you can live in comfort.</p>
                    <p>Comfort isn't just exclusive to your suite, Capital is filled with amenities that let you live comfortably: Bike lockups, Parcel pickup, Coffee bar, Rooftop patio, Fitness Centre and more!</p> -->
                </div>
            </div>
        </div>
    </div>
    <?PHP 
        $fslides = get_field('feature_slides');
        if(!empty($fslides)){

    ?>
    <div class="fslider-container">
        <div class="container">
            <div class="swiper-container feature-slider">
                <div class="swiper-wrapper">

                <?php 
                    foreach($fslides as $aslide){
                        $simgraw = $aslide['image'];
                        $simgstring = '';
                        $altstring = '';
                        if(!empty($simgraw)){
                            $simgurl = $simgraw['url'];
                            $simgtitle = $simgraw['title'];
                            $altstring = !empty($simgraw['alt']) ? $simgraw['alt'] : $aslide['title'];
                            $simgstring = '<img src="'.$simgurl.'" alt="'.$altstring.'">';
                            
                        }
                        $astitle = $aslide['title'];
                        $astext = $aslide['desc'];

                        ?>
                        <div class="swiper-slide aslide">
                            <div class="slidewrap">
                                <div class="bg"><?PHP echo $simgstring; ?></div>
                                <div class="text-container">
                                    <h3><?PHP echo $astitle; ?></h3>
                                    <div class="text">
                                        <?PHP echo $astext; ?>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <?PHP
                    }
                
                ?>
                    

                </div>
            </div>
            <div class="fslide-controls">
                <div class="fsnav nleft fslide-left">
                    <?PHP //get_template_part('components/arrowpager'); ?>
                </div>
                <div class="fspag fslide-paginator">
                    pagination code
                </div>
                <div class="fsnav nright fslide-right"></div>
            </div>
            <script>
                var featSwiper = new Swiper('.feature-slider', {
                    // Optional parameters
                    loop: true,
                    //speed: 1000,
                    spaceBetween: 10,
                    slidesPerView: 2,
                    breakpoints: {
                        // when window width is >= 480px
                        480: {
                        slidesPerView: 2,
                        spaceBetween: 10
                        },
                        768: {
                        slidesPerView: 3,
                        spaceBetween: 30,
                        },
                        
                    },
                    navigation: {// Navigation arrows
                        nextEl: '.fslide-right',
                        prevEl: '.fslide-left',
                    },
                    pagination: {
                        el: '.fslide-paginator',
                        clickable: true,
                    },
                });

            </script>
        </div>
    </div>
    <?PHP 
        } //end feature slides check
    ?>
</section>