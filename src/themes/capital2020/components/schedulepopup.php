<div class="modal fade" id="scheduletour" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <img src="<?PHP echo get_stylesheet_directory_uri().'/images/exit.svg'; ?>" alt="">
        </button>
      </div>
      <div class="modal-body">
            <div class="bg">
                <div class="capital"></div>
            </div>
            <div class="formcontainer">
                <div class="logo-container">
                  <?PHP get_template_part('components/logostack');?> 
                </div> 
                <div class="heading">
                  <h2>Ready to schedule a tour? 
                    <br>
                    Let's get started.
                  </h2>
                </div>
                <?PHP echo do_shortcode('[gravityforms id="1" title="false" description="false" ajax="true"]'); ?>
            </div>
      </div>
      
    </div>
  </div>
</div>