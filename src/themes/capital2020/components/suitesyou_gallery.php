<?php 
    $suiteslides = get_field('suite_slides');
    if(!empty($suiteslides)){

?>
<section class="suitesgallery" id="suitesgallery">
<div class="swiper-container suites-slider">
    <div class="swiper-wrapper">

    <?php 
        foreach($suiteslides as $aslide){
            $simgraw = $aslide['image'];
            $simgstring = '';
            $altstring = '';
            if(!empty($simgraw)){
                $simgurl = $simgraw['url'];
                $simgtitle = $simgraw['title'];
                $altstring = !empty($simgraw['alt']) ? $simgraw['alt'] : $aslide['title'];
                $simgstring = '<img src="'.$simgurl.'" alt="'.$altstring.'">';
                
            }
            $astitle = $aslide['title'];
            if(!empty($aslide['reference'])){
                $astext = ' / '.$aslide['reference'];
            }

            ?>
            <div class="swiper-slide aslide">
                <div class="slidewrap">
                    <div class="bg"><?PHP echo $simgstring; ?></div>
                    <div class="text-container">
                        <div class="textwrap">
                            <span><?PHP echo $astitle; ?></span>
                            <span class="gold">
                                <?PHP echo $astext; ?>
                            </span>

                        </div>
                    </div>
                </div>
                
            </div>
            <?PHP
        }
    
    ?>
        

    </div>
</div>
            <div class="fslide-controls">
                <div class="fsnav nleft sslide-left">
                    <?PHP //get_template_part('components/arrowpager'); ?>
                </div>
                <div class="fspag sslide-paginator">
                </div>
                <div class="fsnav nright sslide-right"></div>
            </div>
            <script>
                var featSwiper = new Swiper('.suites-slider', {
                    // Optional parameters
                    loop: true,
                    speed: 1000,
                    spaceBetween: 10,
                    
                    navigation: {// Navigation arrows
                        nextEl: '.sslide-right',
                        prevEl: '.sslide-left',
                    },
                    pagination: {
                        el: '.sslide-paginator',
                        clickable: true,
                    },
                });

            </script>
</section>

<?php
    } //end suite slides
?>