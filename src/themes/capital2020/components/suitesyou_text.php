<div class="container suitestext-container">
    <div class="scol-wrap">
        <div class="scol scol-4 heading-col">
            <div class="text">
                <h2>Apartments that suit you.</h2>
                <div class="link-container">
                    <?PHP 
                    //generate button to item or page based on where it's included
                    $suid = get_field('suites_page','options');
                    if(!empty($suid)){
                        $suitesl = get_the_permalink($suid);
                        $suoptions = array(
                            'attributes' =>'id="suites-page"',
                        );
                        echo _cptheme_buildbtn($suitesl,'View Floorplans','',$suoptions);
                    }
                    ?>  
                </div>

            </div>
        </div>
        <div class="scol scol-8 desc-col">
            <div class="text-container description sp">   
                <?PHP 
                $suitesummary_raw = get_field('suitetext');
                $suitesummary = apply_filters('the_content',$suitesummary_raw);
                echo $suitesummary;
                ?>
                <!-- <p>Each morning, stretch out in your king size bed and enjoy the contemporary  features at Capital. Each suite is pet friendly and includes walk-in closets, in suite laundry and spacious layouts so you can live in comfort.</p>
                <p>Comfort isn't just exclusive to your suite, Capital is filled with amenities that let you live comfortably: Bike lockups, Parcel pickup, Coffee bar, Rooftop patio, Fitness Centre and more!</p> -->
            </div>
        </div>
    </div>
</div>