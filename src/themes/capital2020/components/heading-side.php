<header class="entry-header boxheader sideheader">
    <div class="heading">
        <div class="container-wide">
            <div class="topwrap">
            <?php
            if ( is_404() ) {
                $title = '<h1>404 Error</h1>';
                echo $title;
            }
            else{
                if ( is_sticky() && is_home() && ! is_paged() ) {
                    printf( '<span class="sticky-post">%s</span>', _x( 'Featured', 'post', '' ) );
                }
                if ( is_singular() ) :
                    the_title( '<h1 class="entry-title">', '</h1>' );
                else :
                    the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                endif;
            }
            
            ?>

            <?PHP 
            $boxlabel = get_field('boxlabel');
            if(!empty($boxlabel)){
                ?>
                <div class="box-container">
                    <div class="box">
                        <?php echo $boxlabel; ?>
                   </div>

                </div>
                <?PHP
            }
            ?>
            </div>
            </div>
    </div>
    
</header><!-- .entry-header -->
