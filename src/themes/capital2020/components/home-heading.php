<section class="top-section home-heading">

<div class="slider-area">

<div class="right-billboard">
    <div class="swiper-container homeslider" id="homeslider">
        <div class="swiper-wrapper">
            <?PHP 
                $homeslides = get_field('home_slides');
                if(!empty($homeslides)){
                    foreach($homeslides as $as){
                        $slide_raw = $as['image'];
                        $slideurl = $slide_raw['url'];
                        $slidetitle = $slide_raw['title'];
                        $altstring = !empty($slide_raw['alt']) ? $slide_raw['alt'] : $slide_raw['title'];
                        $imgstring = '<img src="'.$slideurl.'" alt="'.$altstring.'">';
                        
                        $slidetitle = $as['title'];
                        $slidedesc = $as['text'];
                        ?>
                        <div class="swiper-slide aslide">
                        <div class="bgwrap">
                            <?PHP echo $imgstring; ?>
                        </div>
                        <div class="text-container">
                            <div class="text-wrap">
                                <div class="heading"><?PHP echo $slidetitle; ?></div>
                                <div class="subtitle">
                                <?PHP echo $slidedesc; ?>
                                </div>
                            </div>
                        </div>

                    </div>
                        <?PHP
                    }

                }
            ?>
            
        </div>
    </div>
    <div class="homepager">
        <div class="pbtn leftbtn">
            <div class="icon left"></div>
        </div><div class="pbtn rightbtn">
            <div class="icon right"></div>
        </div>
    </div>
    <script>
    jQuery(document).ready(function () {
        var fpSwiper = new Swiper ('.homeslider', {
            // Optional parameters
            loop: true,
            // Navigation arrows
            effect: 'fade',
            speed: 1000,
            autoplay: {
                delay: 2500,
                disableOnInteraction: true,
            },
            navigation: {
                nextEl: '.leftbtn',
                prevEl: '.rightbtn',
            },
            // runCallbacksOnInit: true,
            // onInit: function(sw){
            //     var track = jQuery('.fptrack');
            //     track.innerHTML = (sw.activeIndex +  1) + '/' + sw.slides.length;
            // },
            // onSlideChangeEnd: function(sw){
            //     var track = jQuery('.fptrack');
            //     track.innerHTML = (sw.activeIndex +  1) + '/' + sw.slides.length + 'Offers';
            // },
        })
    });

    </script>



    <div class="left-items">
        
        <div class="scrolldown">
            <span class="text">Scroll Down</span>
            <div class="aicon"></div>
        </div>
    </div>
</div>
    
</div>
<div class="comfort-section">
    <div class="container">
    <div class="scol-wrap">
            <div class="scol scol-7">
            <div class="text-container">
                <h2>Stepping into your <br>comfort zone</h2>
                <div class="description ind">
                    <?PHP 
                    $introtext_raw = get_field('introtext');
                    $introtext = apply_filters('the_content',$introtext_raw);
                    echo $introtext;
                    ?>
                </div>
            </div>
            </div>
            <div class="scol scol-5">
                <div class="imgwrap">
                    <?php 
                    $intro_image = get_field('introimage');
                    if(!empty($intro_image)){
                        echo _cptheme_buildImage($intro_image);
                    }
                    else{
                        ?><img src="<?PHP echo get_stylesheet_directory_uri().'/images/exterior_render.png'; ?>" alt="Capital Image"><?php
                    }
                    ?>
                    
                </div>
            </div>
        </div>
        
        
    </div>
    
</div>

</section>
