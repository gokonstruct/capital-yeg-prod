<section class="nearby">

<div class="container">
    <div class="scol-wrap">
    
    <div class="scol scol-5 text-col">
        <div class="text">
            <h3>Take your life <br>
            Where it leads you.</h3>
            <div class="description ind sp">
                <?php
                $ictext = get_field('ictext'); 
                echo apply_filters('the_content', $ictext);
                ?>
            </div>
        </div>
        <div class="link-container">
        <?PHP 
            $locid = get_field('location_page','options');
            if(!empty($locid)){
                $locationsl = get_the_permalink($locid);
                $lattrs = array(
                    'attributes'=> 'id="location-page"',
                );
                echo _cptheme_buildBtn($locationsl,"See What's Nearby",'',$lattrs);
            } 
            ?>
        </div>
    </div>
    <div class="scol scol-7">
        <?php 
            //get building features
            $sfeatures = get_field('suites'); //features
            if(!empty($sfeatures)){
                
                ?>
            <div class="include-box" id="features">
                <div class="wrap">
                    <h3>All suites include:</h3>
                    <ul>
                    <?PHP 
                    foreach($sfeatures as $af){
                        echo '<li>'.$af['label'].'</li>';
                    }
                    ?>
                    </ul>
                </div>
            </div>
            <?php
            }
            ?>
    </div>
    </div>
</div>
</section>