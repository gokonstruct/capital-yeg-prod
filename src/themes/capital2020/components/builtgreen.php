<?PHP 
$bgfile = get_field('bgimage');
$bgstring = _cptheme_buildImage($bgfile);
$imgfile = get_field('fpimg');
$imgstring = _cptheme_buildImage($imgfile);
$title = get_field('bgheading');
$text = get_field('bgtext');
?>
<section class="builtgreen" id="builtgreen">
    <div class="bgimg">
        <?php echo $bgstring; ?>
    </div>
    <div class="centerbox">
        <div class="container-wide">
            <div class="white-wrapper">
                <div class="scol-wrap sc-middle">
                    <div class="scol scol-5">
                        <div class="imgwrap">
                            <?PHP 
                            echo $imgstring;
                            ?>
                        </div>
                    </div>
                    <div class="scol scol-7">
                        <h2><?PHP echo $title; ?></h2>
                        <div class="text-container">
                        <?php 
                        if(!empty($text)){

                            echo apply_filters('the_content',$text);
                        }
                        ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</section>