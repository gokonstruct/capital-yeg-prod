<?PHP 
    //featured  image
    
?>
<header class="entry-header imageheader">
    <div class="imgwrapper">
        <?PHP 
        //get feature image
        $imageurl = get_the_post_thumbnail_url( $post );
        $imagealt = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
        if(!empty($imagealt)){
            //if empty get image title
            $imagealt = get_post(get_post_thumbnail_id())->post_title; //The Title
        }
        ?>
        <div class="bgwrap">
            <img src="<?PHP echo $imageurl; ?>" alt="<?PHP echo $imagealt; ?>">
        </div>

        <div class="heading">
            <div class="text-wrap">

                <?php
            if ( is_sticky() && is_home() && ! is_paged() ) {
                printf( '<span class="sticky-post">%s</span>', _x( 'Featured', 'post', '' ) );
            }
            if ( is_singular() ) :
                the_title( '<h1 class="entry-title">', '</h1>' );
                else :
                    the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                endif;
                ?>
            </div>
        </div>
    </div>
    <div class="text-container">
        <div class="container">
            <div class="textwrap">
                <?PHP 
                    the_content();
                ?>
            
            </div>
        </div>
    </div>
</header><!-- .entry-header -->