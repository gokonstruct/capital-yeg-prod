<?php
// footer links on specialized pages

$suitebutton = '';
$buildingbutton = '';
//get links for suites, building, back to home
$incentiveid = get_field('incentives_page','options');
if(!empty($incentiveid)){
    $incentivelink = get_the_permalink($incentiveid);
    $inattrs = array(
        'attributes'=> 'id="incentives-page-popup"',
    );
    $incentivebutton = _cptheme_buildBtn($incentivelink,"View Incentives",'blue',$inattrs);
} 
$amenitiesid = get_field('ammenities_page','options');
if(!empty($amenitiesid)){
    $amlink = get_the_permalink($amenitiesid);
    $battrs = array(
        'attributes'=> 'id="amenities-page-popup"',
    );
    $amenitiesbutton = _cptheme_buildBtn($amlink,"View Amenities",'blue',$battrs);
} 

//contactid
$contactid = get_field('contact_page','options');
if(!empty($contactid)){
    $conlink = get_the_permalink($contactid);
    $cattrs = array(
        'attributes'=> 'id="contact-page-popup"',
    );
    $contactbutton = _cptheme_buildBtn($conlink,"Contact Us",'red',$cattrs);
} 

?>

<div class="link-footer">
    <div class="text">
    Learn more about Capital Apartment's exclusive rental incentives:
    </div>
    <div class="link-container">
        <ul class="hlist-style linklist">
          <?php 
          if(!empty($incentiveid)){
            ?><li>
                <?php echo $incentivebutton; ?>
            </li><?PHP
          }
          ?>
        </ul>
    </div>
   
</div>