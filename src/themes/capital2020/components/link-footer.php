<?php
// footer links on specialized pages

$suitebutton = '';
$buildingbutton = '';
//get links for suites, building, back to home
$suitesid = get_field('suites_page','options');
if(!empty($suitesid)){
    $suiteslink = get_the_permalink($suitesid);
    $sattrs = array(
        'attributes'=> 'id="suites-page"',
    );
    $suitebutton = _cptheme_buildBtn($suiteslink,"View Suites",'',$sattrs);
} 
$buildingid = get_field('ammenities_page','options');
if(!empty($buildingid)){
    $buildinglink = get_the_permalink($buildingid);
    $battrs = array(
        'attributes'=> 'id="building-page"',
    );
    $buildingbutton = _cptheme_buildBtn($buildinglink,"View Building",'',$battrs);
} 

//back to home
$homeattrs = array(
    'attributes' => 'id="home-page"'
);
$siteurl = get_site_url();
$sitebutton = _cptheme_buildBtn($siteurl, "Back to Home", 'red',$homeattrs);


?>

<div class="link-footer">
    <div class="text">
    Explore our suites to discover how you can live comfortably in the heart of downtown Edmonton.
    </div>
    <div class="link-container">
        <ul class="hlist-style linklist">
            <li>
                <?php echo $suitebutton; ?>
            </li><li>
                <?php echo $buildingbutton; ?>
            </li><li>
                <?php echo $sitebutton; ?>
            </li>
        </ul>
    </div>
</div>