<?PHP 
    $agallery = get_field('gallery');
    if(!empty($agallery)){
?>
<section class="section gallery-section gallery-slider" id="gallery">
    <div class="container">
    <div class="gallery-wrapper">
        
        <div class="swiper-container gallery-slide">
            <div class="swiper-wrapper">
            <?PHP foreach($agallery as $ag){
                $ag_img_raw = $ag['image'];
                $ag_img = $ag_img_raw['url'];
                $agtitle = $ag_img_raw['title'];
                $imgstring = '<img src="'.$ag_img.'" alt="'.$agtitle.'" title="'.$agtitle.'" />';
                
                ?>
                <div class="swiper-slide aslide">
                    <div class="imgwrap">
                        <a href="<?PHP echo $ag_img; ?>" data-toggle="lightbox" data-gallery="building-gallery" >
                            <?PHP echo $imgstring; ?>
                        </a>
                    </div>
                </div>
                <?PHP
            }
            ?>
            </div>
        </div>
        <div class="nav-section">
            <h3>Building Gallery</h3>
            <div class="fslide-controls">
                <div class="fsnav nleft bleft"></div>
                <div class="fspag bpag"></div>
                <div class="fsnav nright bright"></div>
            </div>
        </div>
        <script>
            var mySwiper = new Swiper ('.gallery-slide', {
            // Optional parameters
            touch: true,
            speed: 1000,
            spaceBetween: 10,
            slidesPerView: 2,
            breakpoints: {
                // when window width is >= 480px
                480: {
                slidesPerView: 2,
                spaceBetween: 10
                },
                768: {
                slidesPerView: 3,
                spaceBetween: 30,
                },
                992: {
                slidesPerView: 4,
                spaceBetween: 30,
                },
                
            },
            // Navigation arrows
            navigation: {
                nextEl: '.bright',
                prevEl: '.bleft',
            },
            
            pagination: {
                el: '.bpag',
            },
        });

        jQuery(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            jQuery(this).ekkoLightbox();
        });

        </script>
    </div>
    </div>
</section>
<?PHP 
    }
?>