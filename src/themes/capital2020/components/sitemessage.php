<?PHP 

if(function_exists('get_field')){
    $sitemessage = get_field('site_message','options');
    if(!empty($sitemessage)){
        $formatsm = apply_filters("the_content", $sitemessage);
        ?>
<div class="alert alert-warning alert-dismissible starthide site-message" role="alert">
    <div class="text">
        <?PHP echo $formatsm; ?>
    </div>
<div class="close-cont">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>
</div>
        <?PHP

    }
}


?>
