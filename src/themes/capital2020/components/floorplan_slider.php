<?PHP 
    $agallery = get_field('gallery');
    
?>
<section class="section gallery-section gallery-slider" id="gallery">
    <div class="container-wide">
    <div class="gheading">
        
        <div class="link-container">
            <?PHP
				$incentiveid = get_field('incentives_page','options');
                if(!empty($incentiveid)){
                    $incentivelink = get_the_permalink($incentiveid);
                    $iattrs = array(
                        'attributes'=> 'id="floorplan-incentive-page"',
                    );
                    echo _cptheme_buildBtn($incentivelink,"View Incentives",'',$iattrs);
                } 
            ?>
            <?php 
            

            $bookattrs = array(
                'attributes'=> 'data-toggle="modal" data-target="#scheduletour" id="schedule-floorplan"',
            );
            echo _cptheme_buildBtn('#','Book a Tour','',$bookattrs);
            ?>
        </div>

        <?php if(!empty($agallery)){ ?>
            <h3>Suite Gallery</h3>
        <?php } ?>
    </div>
    
    <?php if(!empty($agallery)){ ?>
    <div class="gallery-wrapper">
        
        <div class="swiper-container gallery-slide">
            <div class="swiper-wrapper">
            <?PHP foreach($agallery as $ag){
                $ag_img_raw = $ag['image'];
                $ag_img = $ag_img_raw['url'];
                $agtitle = $ag_img_raw['title'];
                $imgstring = '<img src="'.$ag_img.'" alt="'.$agtitle.'" title="'.$agtitle.'" />';
                ?>
                <div class="swiper-slide aslide">
                    <div class="imgwrap">
                    <a href="<?php echo $ag_img; ?>" data-toggle="lightbox" data-gallery="floorplan-gallery"  >
                        <?PHP echo $imgstring; ?>
                    </a>
                    </div>
                </div>
                <?PHP
            }
            ?>
            </div>
        </div>
        <div class="nav-section">
            <div></div>
            <div class="fslide-controls">
                <div class="fsnav nleft fleft"></div>
                <div class="fspag fpag"></div>
                <div class="fsnav nright fright"></div>
            </div>


        </div>
        <script>
            var mySwiper = new Swiper ('.gallery-slide', {
            // Optional parameters
            touch: true,
            speed: 1000,
            spaceBetween: 10,
            slidesPerView: 2,
            breakpoints: {
                // when window width is >= 480px
                480: {
                slidesPerView: 2,
                spaceBetween: 10
                },
                768: {
                slidesPerView: 3,
                spaceBetween: 30,
                },
                992: {
                slidesPerView: 4,
                spaceBetween: 30,
                },
                
            },
            // Navigation arrows
            navigation: {
                nextEl: '.fright',
                prevEl: '.fleft',
            },
            
            pagination: {
                el: '.fpag',
                clickable: true,
            },
        });

        jQuery(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            jQuery(this).ekkoLightbox();
        });

        </script>
    </div>

    <?PHP 
        } //end gallery check
    ?>
    </div>
</section>