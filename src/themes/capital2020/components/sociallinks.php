<div class="social-wrapper">
    <ul class="slinks">
        <li><a href="https://www.facebook.com/CapitalYEG/" id="facebook-footer-link" class="sbtn facebook-social" title="facebook"><i class="fab fa-facebook-f"></i></a></li>
        <li><a href="https://www.instagram.com/capitalyeg/" id="instagram-footer-link" class="sbtn instagram-social" title="instagram"><i class="fab fa-instagram"></i></a></li>
    </ul>
</div>