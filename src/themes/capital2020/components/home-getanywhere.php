<?php
//Splash version is file download. Full site is link to amenity page
?>
<section class="yourlife" id="map">
<div class="container">
    <div class="scol-wrap">
        <div class="scol scol-5 text-col">
        <div class="text">
            <h2>Take your life <br>where it leads you.</h2>
            <div class="description ind sp">
            <?PHP 
                $maptext_raw = get_field('maptext');
                $maptext = apply_filters('the_content',$maptext_raw);
                echo $maptext;
                ?>
            </div>
        </div>
        <div class="link-container">
        <?PHP 
            $locid = get_field('location_page','options');
            if(!empty($locid)){
                $locationsl = get_the_permalink($locid);
                $lattrs = array(
                    'attributes'=> 'id="location-page"',
                );
                echo _cptheme_buildBtn($locationsl,"See What's Nearby",'',$lattrs);
            } 
            ?>
        </div>
        
        </div>
        <div class="scol scol-7 map-col">
            <img src="<?PHP echo get_stylesheet_directory_uri().'/images/Capital-Location-Map.jpg'; ?>" alt="Capital in Edmonton Map">
        </div>
    </div>
</div>
</section>