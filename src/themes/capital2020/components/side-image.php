<?PHP 
    //get feature image for side menu format
    $imageurl = get_the_post_thumbnail_url( $post );
    if(empty($imageurl)){
        $imageurl = get_stylesheet_directory_uri().'/images/capital-side.png';
    }
    //check if still empty...
    if(!empty($imageurl)){

        $imagealt = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
        if(!empty($imagealt)){
            //if empty get image title
            $imagealt = get_post(get_post_thumbnail_id())->post_title; //The Title
        }
    ?>
<div class="entry-image desktop-lg">
    <div class="imgwrap">
        <img src="<?PHP echo $imageurl; ?>" alt="<?PHP echo $imagealt; ?>">
    </div>
</div>
<?PHP } //end check img ?>