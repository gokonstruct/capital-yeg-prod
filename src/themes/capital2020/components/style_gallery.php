<?php
$styleslides = get_field('style_gallery');
if(!empty($styleslides)){

?>
<section class="stylegallery" id="style">
    <div class="swiper-container sg-gallery">
        <div class="swiper-wrapper">
        
        <?php 
        foreach($styleslides as $as){
            $simgraw = $as['image'];
            $simgstring = '';
            $altstring = '';
            if(!empty($simgraw)){
                $simgurl = $simgraw['url'];
                $simgtitle = $simgraw['title'];
                $altstring = !empty($simgraw['alt']) ? $simgraw['alt'] : $aslide['title'];
                $simgstring = '<img src="'.$simgurl.'" alt="'.$altstring.'">';
                
            }

            //get quote area
            $quote_start = $as['quote'];
            $quote_mid = $as['quotebody'];
            $quote_end = $as['endquote'];

            $ascaption = $as['caption'];
            $asref = '';
            if(!empty($as['ref'])){
                $asref = ' / '.$as['ref'];
            }

            ?>
            <div class="swiper-slide aslide">
            
            <div class="image-wrapper">
                <div class="imgwrap">
                    <?PHP echo $simgstring; ?>
                </div>
                <?PHP 
                if(!empty($quote_start) ||!empty($quote_mid) || !empty($quote_end)){
                ?>
                <div class="text-container">
                    <div class="wrapper">
                        <?PHP 
                        if(!empty($quote_start)){
                        ?><div class="quote">
                       <?PHP echo $quote_start; ?>
                        </div>
                        <?PHP
                        } //end start
                            ?>
                        <?PHP 
                        if(!empty($quote_mid)){
                        ?><div class="text">
                        <?PHP echo $quote_mid; ?>
                        </div>
                        <?PHP
                        } //end mid
                            ?>
                        <?PHP 
                        if(!empty($quote_end)){
                        ?><div class="conclude"><?PHP echo $quote_end; ?></div>
                        <?PHP
                        } //end quoteend chekc
                            ?>
                    </div>
                </div>
                    <?PHP } //end caption check ?>
            </div>
            <div class="bottom-section">
                
                    <div class="text-container">
                        <?php echo $ascaption; ?>
                        <span class="gold">
                        <?php echo $asref; ?>
                        </span>
                    </div>
                    
            </div>
        </div>
            <?PHP

        }
        ?>
        


        </div>
    </div>
    
    <div class="fslide-controls sg-controls">
        <div class="fsnav nleft sgprev"></div>
        <div class="fspag sgpager"></div>
        <div class="fsnav nright sgnext"></div>
    </div>

    <script>
            var mySwiper = new Swiper ('.sg-gallery', {
            // Optional parameters
            touch: true,
            speed: 1000,
            // Navigation arrows
            navigation: {
                nextEl: '.sgnext',
                prevEl: '.sgprev',
            },
            
            pagination: {
                el: '.sgpager',
                clickable: true,
            },
        });

        </script>
</section>
<?php 
} //end check style gallery
?>