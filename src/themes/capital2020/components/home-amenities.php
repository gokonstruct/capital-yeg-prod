<section class="amenities-section" id="amenities">
<div class="container">
    <div class="scol-wrap">
        <div class="scol scol-4 heading-col">
            <div class="text">
                <h2>Eat, Sleep, <br>Rooftop Patio, <br>
                Repeat.</h2>
                <div class="link-container">
                    <?PHP 
                    //generate button to item or page based on where it's included
                    $amid = get_field('ammenities_page','options');
                    if(!empty($amid)){
                        $amenitiesl = get_the_permalink($amid);
                        $amoptions = array(
                            'attributes' =>'id="amenities-page"',
                        );
                        echo _cptheme_buildbtn($amenitiesl,'View Building Amenities','',$amoptions);
                    }
                    ?>  
                </div>

            </div>
        </div>
        <div class="scol scol-8 desc-col">
            <div class="text-container description sp">   
                <?PHP 
                $am_summary_raw = get_field('amenities_text');
                $amneitiessummary = apply_filters('the_content',$am_summary_raw);
                echo $amneitiessummary;
                ?>
            </div>
        </div>
    </div>
</div>
<?PHP 
    $fslides = get_field('feature_slides');
    if(!empty($fslides)){
?>
<div class="fslider-container">
    <div class="container">

    <div class="amenity-heading">
        <h4>Amenities</h4>
        <div class="fslide-controls">
            <div class="fsnav nleft fslide-left"></div>
            <div class="fspag fslide-paginator">
                pagination code
            </div>
            <div class="fsnav nright fslide-right"></div>
        </div>
    </div>
    <div class="swiper-container feature-slider">
    <div class="swiper-wrapper">

    <?php 
        foreach($fslides as $aslide){
            $simgraw = $aslide['image'];
            $simgstring = '';
            $altstring = '';
            if(!empty($simgraw)){
                $simgurl = $simgraw['url'];
                $simgtitle = $simgraw['title'];
                $altstring = !empty($simgraw['alt']) ? $simgraw['alt'] : $aslide['title'];
                $simgstring = '<img src="'.$simgurl.'" alt="'.$altstring.'">';
                
            }
            $astitle = $aslide['title'];
            $astext = $aslide['desc'];

            ?>
            <div class="swiper-slide aslide">
                <div class="slidewrap">
                    <div class="bg"><?PHP echo $simgstring; ?></div>
                    <div class="text-container">
                        <h3><?PHP echo $astitle; ?></h3>
                        <div class="text">
                            <?PHP echo $astext; ?>
                        </div>
                    </div>
                </div>
                
            </div>
            <?PHP
        }
    
    ?>
        

    </div>
</div>

<script>
    var featSwiper = new Swiper('.feature-slider', {
        // Optional parameters
        loop: true,
        //speed: 1000,
        spaceBetween: 10,
        slidesPerView: 2,
        breakpoints: {
            // when window width is >= 480px
            480: {
            slidesPerView: 2,
            spaceBetween: 10
            },
            768: {
            slidesPerView: 3,
            spaceBetween: 30,
            },
            
        },
        navigation: {// Navigation arrows
            nextEl: '.fslide-right',
            prevEl: '.fslide-left',
        },
        pagination: {
            el: '.fslide-paginator',
            clickable: true,
        },
    });

</script>

    </div>
</div>


<?PHP 
    } //end feature slides check
?>

</section>

