<section class="yourlife" id="map">
<div class="container">
    <div class="scol-wrap">
        <div class="scol scol-5 text-col">
        <div class="text">
            <h2>Take your life <br>where it leads you.</h2>
            <div class="description ind sp">
            <?PHP 
                $maptext_raw = get_field('maptext');
                $maptext = apply_filters('the_content',$maptext_raw);
                echo $maptext;
                ?>
            </div>
        </div>
        <div class="link-container">
        <?PHP 
            $amenitiesl = get_field('amenities_file','options');
            if(!empty($amenitiesl)){
                $aattrs = array(
                    'attributes'=> 'target="_blank" id="map-download"',
                );
                echo _cptheme_buildBtn($amenitiesl,'Download Map','red',$aattrs);
            } 
            ?>
        </div>
        
        </div>
        <div class="scol scol-7 map-col">
            <img src="<?PHP echo get_stylesheet_directory_uri().'/images/Capital-Location-Map.jpg'; ?>" alt="Capital in Edmonton Map">
        </div>
    </div>
</div>
</section>