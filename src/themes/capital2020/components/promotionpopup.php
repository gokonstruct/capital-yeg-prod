<?PHP 
    $promotext = get_field('promotext','options');
    $promoimg_raw = get_field('promoimage','options');
    if(!empty($promotext)){

    
?>
<div class="modal fade" id="promotionpop" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <svg viewBox="0 0 18 18" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
          <g id="Exit">
            <g id="Exit">
              <path d="M17.7785 0L0 17.7785" transform="translate(0.1258174 0.01753934)" id="Path-8" fill="none" stroke="" stroke-width="2.42" stroke-linecap="round" stroke-linejoin="round" />
              <path d="M17.7785 0L0 17.7785" transform="matrix(-1 0 0 1 17.90427 0.01753934)" id="Path-8-Copy" fill="none" stroke="" stroke-width="2.42" stroke-linecap="round" stroke-linejoin="round" />
            </g>
          </g>
        </svg>
        </button>
      </div>
      <div class="modal-body">
          <?PHP 
            if(!empty($promoimg_raw)){
                $pimg = $promoimg_raw['url'];
                $pititle = $promoimg_raw['title'];
                ?>
                <div class="imagewrap">
                    <img src="<?PHP echo $pimg; ?>" alt="<?PHP echo $pititle; ?>">
                </div>
                <?PHP 
            }
          ?>
            <div class="content">
              <div class="wrapper">
                <?PHP echo apply_filters('the_content',$promotext); ?>
              </div>
              <?php get_template_part('components/promo-links');?>
            </div> 
  <!-- endcontent -->

      </div>
      
    </div>
  </div>
</div>
<?PHP
    } // end 
?>