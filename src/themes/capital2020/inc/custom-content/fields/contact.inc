<?php
//fields for grid layout

if( function_exists('acf_add_local_field_group') ):


acf_add_local_field_group(array(
	'key' => 'cpts_contact_fields',
	'title' => 'Contact Fields',
	'fields' => array(
        
		
		
	),
	'location' => array(
		array(
			array(
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'templates/tpl-contact.php',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;
