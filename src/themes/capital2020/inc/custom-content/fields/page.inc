<?php
if( function_exists('acf_add_local_field_group') ):
//Page Fields

acf_add_local_field_group(array(
	'key' => 'cpt_page_fields',
	'title' => 'Page Fields',
	'fields' => array(

		array(
			'key' => 'field_cp_page_headerbox',
			'label' => 'Box Label',
			'name' => 'boxlabel',
			'type' => 'text',
			'instructions' => 'On certain templates for use on a gold box with text',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
            ),
            array(
				'param' => 'page_template',
				'operator' => '!=',
				'value' => 'templates/tpl-homepage.php',
            ),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

//shared post and page fields

acf_add_local_field_group(array(
	'key' => 'bdt_postpage_fields',
	'title' => 'Page and Post Fields',
	'fields' => array(


	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
            array(
				'param' => 'page_template',
				'operator' => '!=',
				'value' => 'templates/tpl-homepage.php',
            ),
		),
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post', //or option
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));



endif;
