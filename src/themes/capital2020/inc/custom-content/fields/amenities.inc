<?php
if( function_exists('acf_add_local_field_group') ):
//Page Fields

acf_add_local_field_group(array(
	'key' => 'cpt_fp_amenities_fields',
	'title' => 'Amenities Fields',
	'fields' => array(

        //content
        array(
			'key' => 'field_cpa_text',
			'label' => 'Lead Text',
			'name' => 'leadtext',
			'type' => 'wysiwyg',
			'instructions' => 'text beside buiding features list',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 1,
			'delay' => 0,
		),
        //building features

        array(
			'key' => 'field_fpa_features_building',
			'label' => 'Buidling Features',
			'name' => 'buildingfeatures',
			'type' => 'repeater',
			'instructions' => 'list of building features',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => '',
			'min' => 0,
			'max' => 0,
			'layout' => 'table',
			'button_label' => 'Add Feature',
			'sub_fields' => array(
				array(
					'key' => 'field_am_b_label',
					'label' => 'Feature Label',
					'name' => 'label',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
			),
        ), //end repeater

		//special gallery
		array(
			'key' => 'field_cp_bd_slides',
			'label' => 'Gallery',
			'name' => 'gallery',
			'type' => 'repeater',
			'instructions' => 'Building Gallery',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => '',
			'min' => 0,
			'max' => 0,
			'layout' => 'table',
			'button_label' => '',
			'sub_fields' => array(
				array(
					'key' => 'field_cp_bd_image',
					'label' => 'Image',
					'name' => 'image',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
				),
				
			),
        ), //end repeater
        
        //pre gallery heading

        array(
            'key' => 'field_cp_bd_middle_text',
            'label' => 'Middle Heading',
            'name' => 'middleheading',
            'type' => 'text',
            'instructions' => 'Heading before slides',
            'required' => 0,
            'default_value' => '',
            'placeholder' => '',
        ),

        //story gallery
		array(
			'key' => 'field_cp_bd_story_slides',
			'label' => 'Style Gallery',
			'name' => 'style_gallery',
			'type' => 'repeater',
			'instructions' => 'Style Gallery',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => '',
			'min' => 0,
			'max' => 0,
			'layout' => 'row',
			'button_label' => '',
			'sub_fields' => array(
                
				array(
					'key' => 'field_cp_st_image',
					'label' => 'Image',
					'name' => 'image',
					'type' => 'image',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
					'min_width' => '',
					'min_height' => '',
					'min_size' => '',
					'max_width' => '',
					'max_height' => '',
					'max_size' => '',
					'mime_types' => '',
                ),
                
                array(
                    'key' => 'field_cp_st_quote',
                    'label' => 'Quote',
                    'name' => 'quote',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'default_value' => '',
                    'placeholder' => '',
                ),
                array(
                    'key' => 'field_cp_st_quotebody',
                    'label' => 'Qupte Body',
                    'name' => 'quotebody',
                    'type' => 'wysiwyg',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'tabs' => 'all',
                    'toolbar' => 'full',
                    'media_upload' => 1,
                    'delay' => 0,
                ),
                array(
                    'key' => 'field_cp_st_conclude',
                    'label' => 'End Quote',
                    'name' => 'endquote',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'default_value' => '',
                    'placeholder' => '',
                ),
				array(
					'key' => 'field_cp_st_caption',
					'label' => 'Bottom Caption',
					'name' => 'caption',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'default_value' => '',
					'placeholder' => '',
                ),
                array(
					'key' => 'field_cp_st_caption_ref',
					'label' => 'Bottom reference',
					'name' => 'ref',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'default_value' => '',
					'placeholder' => '',
				),
			),
		), //end repeater
	),
	'location' => array(
		array(
			array(
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'templates/tpl-amenities.php',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));



endif;
