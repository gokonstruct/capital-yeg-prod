<?php
/**
 * The template for displaying the footer
 */

?>
<?PHP get_template_part('components/promotionpopup'); ?>
<?PHP get_template_part('components/schedulepopup'); ?>
<footer id="footer" class="site-footer">
    
    <div class="top-footer">
        <div class="container-wide">
           
            <div class="site-info">
                <div class="scol-wrap info-wrap" id="contact">
                    <div class="scol scol-7 book-col">
                            <h2>Rental apartments that let you <span class="italic">Capitalize</span> your living space.</h2>
                            <div class="link-container">
                                <div class="schedule">
                                <?PHP 
                                    $schattrs = array(
                                        'attributes'=> 'data-toggle="modal" data-target="#scheduletour" id="schedule-footer"',
                                    );
                                    echo _cptheme_buildBtn('','Book Your Tour Today','blue',$schattrs);
                                ?>
                                </div>
                                <div class="phone">
                                    <?PHP 
                                        //get phone
                                    if(function_exists('get_field')){
                                        $phone = get_field('phone','options'); //587.747.5227
                                        if(!empty($phone)){
                                            $pattrs = array(
                                                'attributes'=> 'id="phone-footerleft"',
                                            );
                                            $pnumber = preg_replace('~\D~', '', $phone);
                                            echo _cptheme_buildBtn('tel:'.$pnumber,$phone,'redoutline',$pattrs);
                                        }
                                    }//end check ge tfield
                                    ?>      
                                </div>
                            </div>
                        </div>
                        <div class="scol scol-5 contact-col">
                            <h3>Contact us today</h3>
                            <div class="scol-wrap info-section">
                                <div class="scol scol-6 first-col">
                                    <div class="location">
                                        <h4>Address</h4>
                                        <div class="address">
                                            <?php 
                                            $addresslink = get_field('addresslink','options');
                                            $address_text = get_field('address','options');
                                            //https://goo.gl/maps/nEuAGfNKfHKt2qfv6
                                            if(!empty($addresslink)){
                                                echo '<a href="'.$addresslink.'" id="address-link">'.$address_text.'</a>';
                                            }
                                            else{
                                                echo $address_text; 
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="contact">
                                         <?PHP 
                                        //get phone
                                        if(function_exists('get_field')){
                                            $phone = get_field('phone','options'); //587.747.5227
                                            if(!empty($phone)){
                                                $pnumber = preg_replace('~\D~', '', $phone);
                                                
                                                ?>
                                                <div><a href="tel:<?PHP echo $pnumber; ?>" class="phone" id="phone-footer"><?PHP echo $phone; ?></a></div>
                                                <?PHP
                                            }
                                            $cemail = get_field('contact_email','options');
                                            if(!empty($cemail)){
                                                $mto = strtolower($cemail);
                                                ?>
                                                <div><a href="mailto:<?PHP echo $mto; ?>" class="email" id="mail-footer"><?PHP echo $cemail; ?></a></div>    
                                                <?PHP
                                            }
                                        }//end check ge tfield
                                        ?>                
                                    </div>
                                    
                                    
                                </div>
                                <div class="scol scol-6 location-col">
                                <div class="top-part">
                                    <div class="hours">
                                        <h4>Leasing Office</h4>
                                        <div class="officehours">
                                            <ul class="timelist">
                                                <?PHP 

                                                    $checkhours = get_field('showoffice','options');

                                                    $hours = array(
                                                        array(
                                                            'day' => 'Mon',
                                                            'time' => '10:00 AM - 4:00PM',
                                                        ),
                                                        array(
                                                            'day' => 'Tues',
                                                            'time' => '10:00 AM - 4:00PM',
                                                        ),
                                                        array(
                                                            'day' => 'Wed',
                                                            'time' => '10:00 AM - 4:00PM',
                                                        ),
                                                        array(
                                                            'day' => 'Thur',
                                                            'time' => '10:00 AM - 4:00PM',
                                                        ),
                                                        array(
                                                            'day' => 'Fri',
                                                            'time' => '10:00 AM - 4:00PM',
                                                        ),
                                                        array(
                                                            'day' => 'Sat',
                                                            'time' => '10:00 AM - 4:00PM',
                                                        ),
                                                        array(
                                                            'day' => 'Sun',
                                                            'time' => '10:00 AM - 4:00PM',
                                                        ),
                                                    );
                                                    $hours = get_field('officehours','options');

                                                
                                                    //option to hide temporarily
                                                    if(!empty($checkhours) && !empty($hours)){
                                                        foreach($hours as $hr){
                                                            $d = $hr['day'];
                                                            $t = $hr['time'];
                                                            echo "<li><span class=\"day\">$d</span> <span class=\"time\">$t</span></li>";
                                                        }
                                                    }
                                                    else{
                                                        echo '<li class="empty">opening soon.</li>';
                                                    }
                                                    
                                                ?>
                                            </ul>
                                            <?php //echo get_field('hours','options'); ?>

                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                </div>
            </div><!-- .site-info -->
        </div>
        </div>
    </div>
    <div class="newsletter-container">
        <div class="nw">
            <div class="signup-wrapper">
                <div class="scol-wrap">
                    <div class="scol scol-5">
                    <h3><span class="gold">Sign up</span> to stay up to date.</h3>
                    </div>
                    <div class="scol scol-7">
                        <div class="embed-form">
                        <?PHP echo do_shortcode('[gravityforms id="2" title="false" description="false" ajax="true"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?PHP 
    //option to hide everything above for certain pages.
    ?>
    <div class="bottom-footer">
        <div class="bc">
            <div class="wrapper">
                <div class="sglogo">
                    <div class="logo">
                    <?PHP get_template_part('components/logo');?>
                    </div>
                    <div class="copy">
                        <?PHP get_template_part('components/strategicgroup');?>
                    </div>
                </div>
                <div class="right">
                    <?php 
                    get_template_part('components/sociallinks');
                    ?>
                    <ul class="flinks hlist-style"> 
                    <?php
                    $tcpage = get_field('tc_page','options');
                    if(!empty($tcpage)){
                        $tcurl = get_the_permalink($tcpage);
                        echo '<li>';
                        echo _cptheme_buildBtn($tcurl,'Terms & Conditions','btn');
                        echo '</li>';

                    }
                    if ( function_exists( 'the_privacy_policy_link' ) ) {
                        echo '<li>';
                        the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
                        echo '</li>';
                    }
                    ?>
                    </ul>
                
                </div>
            </div>
        </div>
    </div> <!-- ENd container -->
</footer><!-- #site-footer -->

<?php wp_footer(); ?>

</body>
</html>
